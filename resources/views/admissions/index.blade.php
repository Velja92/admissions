@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Description</th>
                    @if($user['role']['role_name'] == 'administrator')
                        <th>Status</th>
                        <th>Pending Applications</th>
                        <th>Active Applications</th>
                        <th>Rejected Applications</th>
                        <th>Edit</th>
                    @endif
                </thead>
                <tbody>
                    @foreach($admissions as $admission)
                        <tr>
                            <td>{{$admission['name']}}</td>
                            <td>{{$admission['description']}}</td>
                            @if($user['role']['role_name'] == 'administrator')
                                <td>{{$admission['active'] ? 'active' : 'inactive'}}</td>
                                <td>{{$admission['countPending']}}</td>
                                <td>{{$admission['countActive']}}</td>
                                <td>{{$admission['inactiveCount']}}</td>
                                <td>
                                    <a href="/admissions/{{$admission['id']}}">
                                        <span class="oi oi-pencil"></span>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if($user['role']['role_name'] == 'administrator')
                <div>
                    <a href="admissions/create">Create Admission Type</a>
                </div>
            @endif
        </div>
    </div>
@endsection