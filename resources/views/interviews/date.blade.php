@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="d-block" style="width: 100%">Interviews for {{$date}}</h1>
            @if(count($available_times) == 7)
                <h2 style="border-top: 1px solid #000;">
                    No scheduled interviews for this date!
                </h2>
            @elseif(count($available_times))
                <table class="table">
                    <thead>
                        <th>Date</th>
                        <th>Time</th>
                        <th>User</th>
                        <th>Admission Type</th>
                    </thead>
                    <tbody>
                        @foreach($interviews as $interview)
                            <tr>
                                <td>{{$interview['date']}}</td>
                                <td>{{$interview['time']}}</td>
                                <td>{{$interview['user']['first_name'] . ' ' . $interview['user']['last_name']}}</td>
                                <td>{{$interview['admission']['name']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1>
                    All interviews are filled!
                </h1>
            @endif
            @if(count($available_times) && $isStudent)
                <div class="d-block" style="width: 100%">
                    <a href="/schedule/create?date={{$date}}">Apply for interview</a>
                </div>
            @endif
        </div>
    </div>
@endsection