@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Date:</h4>
                <span>{{$interview['date']}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Time:</h4>
                <span>{{$interview['time']}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Status:</h4>
                <span>{{$interview->getStatus()}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Type:</h4>
                <span>{{$interview['admission']['name']}}</span>
            </div>
            <div class="col-lg-12">
                @if($interview['status'] === 0)
                    <form action="/interview/accept/{{$interview['id']}}" method="POST" class="d-inline-block">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Accept
                                </button>
                            </div>
                        </div>
                    </form>
                    <form action="/interview/reject/{{$interview['id']}}" method="POST" class="d-inline-block">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Reject
                                </button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection