@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(!count($interviews))
                <h1>
                    You have now scheduled interviews!
                </h1>
            @else
                <table class="table">
                    <thead>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Admission Type</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Edit</th>
                    </thead>
                    <tbody>
                        @foreach($interviews as $interview)
                            <tr>
                                <td>{{$interview['date']}}</td>
                                <td>{{$interview['time']}}</td>
                                <td>{{$interview['admission']['name']}}</td>
                                <td>{{$interview['user']['first_name'] . ' ' . $interview['user']['last_name']}}</td>
                                <td>{{$interview->getStatus()}}</td>
                                <td>
                                    <a href="/interview/{{$interview['id']}}">
                                        <span class="oi oi-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection