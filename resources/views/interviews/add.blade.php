@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(count($available_times) && count($available_interviews))
                <form class="col-lg-12" method="POST" action="/interview" aria-label="{{ __('Create') }}">
                    @csrf
                    <input type="hidden" name="date" value="{{$date}}">
                    <div class="form-group row">
                        <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>
                        <div class="col-md-6">
                            <select name="time" id="time" class="form-control">
                                @foreach($available_times as $time)
                                    <option value="{{$time}}">{{$time}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Interview') }}</label>
                        <div class="col-md-6">
                            <select name="admission_type_id" id="admission_type_id" class="form-control">
                                @foreach($available_interviews as $interview)
                                    <option value="{{$interview['id']}}">{{$interview['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Create') }}
                            </button>
                        </div>
                    </div>
                </form>
            @elseif(count($available_times))
                <h1>
                    You have already scheduled all of your interviews!
                </h1>
            @else
                <h1>
                    All times for this date are reserved!
                </h1>
            @endif
        </div>
    </div>
@endsection