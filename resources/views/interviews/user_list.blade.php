@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(!count($interviews))
                <h1>
                    You have no scheduled interviews!
                </h1>
            @else
                <h1 class="text-center">My interviews</h1>
                <table class="table">
                    <thead>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Admission Type</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        @foreach($interviews as $interview)
                            <tr>
                                <td>{{$interview['date']}}</td>
                                <td>{{$interview['time']}}</td>
                                <td>{{$interview['admission']['name']}}</td>
                                <td>{{$interview->getStatus()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection