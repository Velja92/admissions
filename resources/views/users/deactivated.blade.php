@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>
                You account has been deactivated, please contact the support service!
            </h1>
        </div>
    </div>
@endsection