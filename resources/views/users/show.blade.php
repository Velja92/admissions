@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Name:</h4>
                <span>{{$user['first_name'] . ' ' . $user['last_name']}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Email:</h4>
                <span>{{$user['email']}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Role:</h4>
                <span>{{$roles[$user['role_id']]}}</span>
            </div>
            <div class="col-lg-12">
                <h4 class="d-inline-block col-lg-1">Status:</h4>
                <span>{{$user['active'] ? 'active' : 'suspended'}}</span>
            </div>
            <div class="col-lg-12">
                <form action="/users/{{$user['id']}}/status" method="POST">
                    @csrf
                    <div class="form-group row mb-0">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">
                                {{$user['active'] ? 'suspend' : 'activate'}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection