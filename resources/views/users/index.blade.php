@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Edit</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user['first_name'] . ' ' .$user['last_name']}}</td>
                            <td>{{$user['email']}}</td>
                            <td>{{$user->role['role_name']}}</td>
                            <td>{{$user['active'] ? 'Active' : 'Suspended'}}</td>
                            <td>
                                <a href="/users/{{$user['id']}}">
                                    <span class="oi oi-pencil"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                <a href="users/create">Create User</a>
            </div>
        </div>
    </div>
@endsection