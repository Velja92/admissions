<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('register', 'RegisterController@show');
Route::post('register', 'RegisterController@create')->name('register');

Route::middleware(['isActive'])->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('schedule', 'InterviewController@getDate');

    Route::middleware(['isStudent'])->group(function () {
        Route::get('admissions/active', 'AdmissionTypeController@activeAdmissions');
        Route::get('schedule/create', 'InterviewController@addInterview');
        Route::post('interview', 'InterviewController@store');
        Route::get('interview/user', 'InterviewController@userList');
    });

    Route::middleware(['isStaff'])->group(function () {
        Route::get('interviews', 'InterviewController@index');
        Route::get('interview/{interview}', 'InterviewController@edit');
        Route::post('interview/reject/{interview}', 'InterviewController@reject');
        Route::post('interview/accept/{interview}', 'InterviewController@accept');
    });

    Route::middleware(['isAdmin'])->group(function () {
        Route::get('users', 'UserController@index');
        Route::get('users/create', 'UserController@create');
        Route::post('users', 'UserController@store');
        Route::get('users/{user}', 'UserController@edit');
        Route::post('users/{user}/status', 'UserController@changeStatus');

        Route::get('admissions', 'AdmissionTypeController@index');
        Route::get('admissions/create', 'AdmissionTypeController@create');
        Route::post('admissions', 'AdmissionTypeController@store');
        Route::get('admissions/{admission}', 'AdmissionTypeController@edit');
        Route::post('admissions/{admission}', 'AdmissionTypeController@update');
    });
});

Route::get('deactivated', function (){
    return view('users.deactivated');
});
