<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Repositories\User\UserRepositoryInterface;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('users.index', ['users' => $this->userRepository->all()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = ['administrator' => Role::ADMINISTRATOR, 'staff' => Role::STAFF];
        return view('users.create', compact('roles'));
    }

    /**
     * @param StoreUser $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(StoreUser $request)
    {
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        if(!$user = $this->userRepository->create($validated)){
            throw new \Exception('Something went wrong with user creation, try again!');
        }
        else{
            return redirect('users');
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = [ Role::ADMINISTRATOR =>'administrator', Role::STUDENT => 'student', Role::STAFF => 'staff'];
        return view('users.show', compact(['roles', 'user']));
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(User $user)
    {
        $this->userRepository->toggleStatus($user);
        return redirect('users');
    }
}
