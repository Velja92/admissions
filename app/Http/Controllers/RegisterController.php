<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Repositories\User\UserRepositoryInterface;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(){
        return view('auth.register');
    }

    /**
     * @param StoreUser $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create(StoreUser $request){
        $validated = $request->validated();
        $validated['role_id'] = Role::STUDENT;
        $validated['password'] = Hash::make($validated['password']);
        if(!$user = $this->userRepository->create($validated)){
            throw new \Exception('Something went wrong with user creation, try again!');
        }

        if(Auth::attempt($request->only(['email', 'password']))){
            return redirect('/home');
        }
        else{
            redirect()->back();
        }
    }
}
