<?php

namespace App\Http\Controllers;

use App\Interview;
use App\Repositories\Interview\InterviewRepositoryInterface;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InterviewController extends Controller
{
    /**
     * @var InterviewRepositoryInterface
     */
    protected $interviewRepository;

    /**
     * InterviewController constructor.
     * @param InterviewRepositoryInterface $interviewRepository
     */
    public function __construct(InterviewRepositoryInterface $interviewRepository)
    {
        $this->interviewRepository = $interviewRepository;
    }

    public function index()
    {
        $interviews = $this->interviewRepository->all();
        return view('interviews.index', compact('interviews'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDate(Request $request){
        $date = $request->get('date');
        $cDate = Carbon::parse($date);
        $errorMessage = '';
        if($cDate->isSunday() || $cDate->isSaturday()){
            $errorMessage = "Can't add interview during weekend";
        }
        if($cDate->isPast()){
            $errorMessage = "Can't add interview for past";
        }
        if($errorMessage){
            return redirect()->back()->with('customError', $errorMessage);
        }

        $available_times = $this->interviewRepository->getAvailableTimes($date);
        $interviews = $this->interviewRepository->getInterviewsByDate($date);
        $isStudent = Auth::user()->getAttribute('role_id') == Role::STUDENT;
        return view('interviews.date', compact(['interviews', 'available_times', 'date', 'isStudent']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addInterview(Request $request)
    {
        $date = $request->get('date');
        $available_times = $this->interviewRepository->getAvailableTimes($date);
        $available_interviews = $this->interviewRepository->getAvailableInterviews();
        return view('interviews.add', compact(['available_times', 'date', 'available_interviews']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $date = $request->post('date');
        $data = $request->all();
        $data['user_id'] = Auth::user()->getAttribute('id');
        try{
            $this->interviewRepository->create($data);
        }
        catch (\Exception $e){
            if($e instanceof QueryException){
                $message = "The date is occupied, please try again";
            }
            else{
                $mesasge = "An error has occurred, please try again!";
            }
            return redirect('/schedule?date=' . $date)->with('customError', $message);
        }

        return redirect('/schedule?date=' . $date);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userList()
    {
        $user = Auth::user();
        $interviews = $this->interviewRepository->getByUser($user);
        return view('interviews.user_list', compact('interviews'));
    }

    /**
     * @param Interview $interview
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Interview $interview)
    {
        return view('interviews.show', compact('interview'));
    }

    /**
     * @param Interview $interview
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reject(Interview $interview)
    {
        $this->interviewRepository->rejectInterview($interview);
        return redirect('interviews');
    }

    /**
     * @param Interview $interview
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accept(Interview $interview)
    {
        $this->interviewRepository->acceptInterview($interview);
        return redirect('interviews');
    }
}
