<?php

namespace App\Http\Controllers;

use App\Repositories\Interview\InterviewRepositoryInterface;
use Illuminate\Http\Request;
use Skecskes\Calendar\Calendar;

class HomeController extends Controller
{
    /**
     * @var InterviewRepositoryInterface
     */
    protected $interviewRepository;

    /**
     * HomeController constructor.
     * @param InterviewRepositoryInterface $interviewRepository
     */
    public function __construct(InterviewRepositoryInterface $interviewRepository)
    {
        $this->interviewRepository = $interviewRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = $this->interviewRepository->getActiveInterviews();
        $events = array();
        foreach ($interviews as $interview){
            $events[$interview['date'] . ' ' . $interview['time']] = array($interview['time']);
        }

        $cal = \Skecskes\Calendar\Facades\Calendar::make();
        $cal->setEvents($events);
        $cal->setStartEndHours(9,15);
        return view('home', compact('cal'));
    }
}
