<?php

namespace App\Http\Controllers;

use App\AdmissionType;
use App\Http\Requests\UpdateAdmissionType;
use App\Repositories\AdmissionType\AdmissionTypeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdmissionTypeController extends Controller
{
    /**
     * @var AdmissionTypeRepositoryInterface
     */
    protected $admissionTypeRepository;

    /**
     * AdmissionTypeController constructor.
     * @param AdmissionTypeRepositoryInterface $admissionTypeRepository
     */
    public function __construct(AdmissionTypeRepositoryInterface $admissionTypeRepository)
    {
        $this->admissionTypeRepository = $admissionTypeRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admissions.index', ['admissions' => $this->admissionTypeRepository->all(), 'user' => Auth::user()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activeAdmissions()
    {
        return view('admissions.index', ['admissions' => $this->admissionTypeRepository->getActiveAdmissions(), 'user' => Auth::user()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admissions.create');
    }

    /**
     * @param UpdateAdmissionType $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UpdateAdmissionType $request)
    {
        $validated = $request->validated();
        $this->admissionTypeRepository->create($validated);
        return redirect('admissions');
    }

    /**
     * @param AdmissionType $admission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(AdmissionType $admission)
    {
        return view('admissions.edit', compact('admission'));
    }

    /**
     * @param UpdateAdmissionType $request
     * @param AdmissionType $admission
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateAdmissionType $request, AdmissionType $admission)
    {
        $validated = $request->validated();
        $this->admissionTypeRepository->update($validated, $admission->getAttribute('id'));
        return redirect('admissions');
    }
}
