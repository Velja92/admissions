<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyIfStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->getAttribute('role_id') !== Role::STAFF){
            return redirect('/home')->with('customError', "You don't have the right permissions to access this route!");
        }

        return $next($request);
    }
}
