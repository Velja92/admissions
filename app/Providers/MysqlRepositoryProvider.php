<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MysqlRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $type = 'Mysql';

        $models = [
            'AdmissionType',
            'Interview',
            'Role',
            'User',
        ];

        foreach ($models as $model) {
            $this->app->bind(
                "App\\Repositories\\{$model}\\{$model}RepositoryInterface",
                "App\\Repositories\\{$model}\\{$model}{$type}Repository"
            );
        }
    }
}
