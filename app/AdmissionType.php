<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionType extends Model
{
    protected $guarded = [];

    protected $appends = ['countActive', 'inactiveCount', 'countPending'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interviews()
    {
        return $this->hasMany(Interview::class);
    }

    /**
     * @return int
     */
    public function getCountActiveAttribute()
    {
        return $this->interviews()->where('status', Interview::ACCEPTED)->count();
    }

    /**
     * @return int
     */
    public function getInactiveCountAttribute()
    {
        return $this->interviews()->where('status', Interview::REJECTED)->count();
    }

    /**
     * @return int
     */
    public function getCountPendingAttribute()
    {
        return $this->interviews()->where('status', Interview::PENDING)->count();
    }
}
