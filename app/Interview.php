<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $guarded = [];

    protected $with = ['user', 'admission'];

    const TIMES = ['09:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00'];

    const PENDING = 0;
    const ACCEPTED = 1;
    const REJECTED = 2;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admission()
    {
        return $this->belongsTo(AdmissionType::class, 'admission_type_id', 'id');
    }

    /**
     * @return string
     */
    public function getStatus(){
        switch ($this->getAttribute('status')){
            case 2: return 'Rejected';break;
            case 1: return 'Accepted';break;
            case 0: return 'Pending';break;
        }
    }
}
