<?php

namespace App\Repositories\AdmissionType;

use App\AdmissionType;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;

class AdmissionTypeMysqlRepository extends Repository implements AdmissionTypeRepositoryInterface
{
    /**
     * AdmissionTypeMysqlRepository constructor.
     * @param AdmissionType $model
     */
    public function __construct(AdmissionType $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritdoc
     */
    public function getActiveAdmissions()
    {
        return $this->model->where('active', true)->get();
    }


}