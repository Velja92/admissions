<?php

namespace App\Repositories\AdmissionType;

use App\Repositories\RepositoryInterface;

interface AdmissionTypeRepositoryInterface extends RepositoryInterface
{
    /**
     * @return mixed
     */
    public function getActiveAdmissions();
}