<?php

namespace App\Repositories\Role;

use App\Repositories\RepositoryInterface;
use App\Role;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories
 */
interface RoleRepositoryInterface extends RepositoryInterface
{

}
