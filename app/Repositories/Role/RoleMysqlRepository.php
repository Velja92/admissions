<?php

namespace App\Repositories\Role;

use App\Role;
use App\Repositories\Repository;

/**
 * Class RoleMysqlRepository
 * @package App\Repositories\Company
 */
class RoleMysqlRepository extends Repository implements RoleRepositoryInterface
{

    /**
     * RoleMysqlRepository constructor.
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

}
