<?php

namespace App\Repositories\User;

use App\Repositories\RepositoryInterface;
use App\User;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * @param User $user
     * @return mixed
     */
    public function toggleStatus(User $user);
}
