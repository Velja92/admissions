<?php

namespace App\Repositories\User;

use App\User;
use App\Repositories\Repository;

/**
 * Class UserMysqlRepository
 * @package App\Repositories\Company
 */
class UserMysqlRepository extends Repository implements UserRepositoryInterface
{

    /**
     * RoleMysqlRepository constructor
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritdoc
     */
    public function toggleStatus(User $user)
    {
        $active = !$user->getAttribute('active');
        $user->setAttribute('active', $active);
        $user->save();
    }

}
