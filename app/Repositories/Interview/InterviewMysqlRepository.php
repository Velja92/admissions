<?php

namespace App\Repositories\Interview;

use App\AdmissionType;
use App\Interview;
use App\Repositories\Repository;
use App\User;
use Illuminate\Support\Facades\Auth;

class InterviewMysqlRepository extends Repository implements InterviewRepositoryInterface
{
    /**
     * InterviewMysqlRepository constructor.
     * @param Interview $model
     */
    public function __construct(Interview $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritdoc
     */
    public function getInterviewsByDate($date)
    {
        return $this->model->where('date', '=', $date)->where('status', '!=', Interview::REJECTED)->get();
    }

    /**
     * @inheritdoc
     */
    public function getAvailableTimes($date)
    {
        $times = $this->model->where('date', '=', $date)->where('status', '!=', Interview::REJECTED)->pluck('time')->toArray();
        return array_diff( Interview::TIMES, $times);
    }

    /**
     * @return mixed
     */
    public function getAvailableInterviews()
    {
        return AdmissionType::whereNotIn('id', Interview::where('user_id', '=', Auth::user()->getAttribute('id'))
            ->whereIn('status', [Interview::ACCEPTED, Interview::PENDING])->pluck('admission_type_id')->toArray())->where('active', '=', true)->get();
    }

    /**
     * @inheritdoc
     */
    public function rejectInterview(Interview $interview)
    {
        $interview->setAttribute('status', Interview::REJECTED);
        $interview->save();
    }

    /**
     * @inheritdoc
     */
    public function acceptInterview(Interview $interview)
    {
        $interview->setAttribute('status', Interview::ACCEPTED);
        $interview->save();
    }

    /**
     * @inheritdoc
     */
    public function getByUser(User $user)
    {
        $userId = $user->getAttribute('id');
        return $this->model->where('user_id', '=', $userId)->get();
    }

    /**
     * @inheritdoc
     */
    public function getActiveInterviews()
    {
        return $this->model->where('status', '!=', Interview::REJECTED)->orderBy('time')->get();
    }


}