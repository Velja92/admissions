<?php

namespace App\Repositories\Interview;

use App\Interview;
use App\Repositories\RepositoryInterface;
use App\User;

interface InterviewRepositoryInterface extends RepositoryInterface
{
    /**
     * @param $date
     * @return mixed
     */
    public function getInterviewsByDate($date);

    /**
     * @param $date
     * @return mixed
     */
    public function getAvailableTimes($date);

    /**
     * @return mixed
     */
    public function getAvailableInterviews();

    /**
     * @param Interview $interview
     * @return mixed
     */
    public function rejectInterview(Interview $interview);

    /**
     * @param Interview $interview
     * @return mixed
     */
    public function acceptInterview(Interview $interview);

    /**
     * @param User $user
     * @return mixed
     */
    public function getByUser(User $user);

    /**
     * @return mixed
     */
    public function getActiveInterviews();
}