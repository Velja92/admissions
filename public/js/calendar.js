(function calendarFunction() {
    let calendar = $('.table.table-calendar.month');
    let date = calendar.find('tbody tr td');
    date.css('cursor', 'pointer');
    date.on('click', function (e) {
        if($(this).data('datetime') != null){
            window.location = 'schedule?date=' +  $(this).data('datetime');
        }
    })
})()