<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('interviews') && Schema::hasTable('users') && Schema::hasTable('admission_types')){
            Schema::table('interviews', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('admission_type_id')->references('id')->on('admission_types');
                $table->unique(['user_id', 'admission_type_id', 'status']);
                $table->unique(['time', 'date', 'status']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('interviews') && Schema::hasTable('users') && Schema::hasTable('admission_types')){
            Schema::table('interviews', function (Blueprint $table) {
                $table->dropForeign('user_id');
                $table->dropForeign('admission_type_id');
            });
        }
    }
}
