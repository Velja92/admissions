<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
           'id' => Role::ADMINISTRATOR,
            'role_name'  => 'administrator'
        ]);

        DB::table('roles')->insert([
            'id' => Role::STUDENT,
            'role_name'  => 'student'
        ]);

        DB::table('roles')->insert([
            'id' => Role::STAFF,
            'role_name'  => 'staff'
        ]);
    }
}
