<?php

use Illuminate\Database\Seeder;

class AdmissionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admission_types')->insert([
            'id' => 1,
            'name' => 'Freshman interview',
            'description' => 'An interview is not a required part of the application process, but we encourage you to meet and talk with student interviewer'
        ]);

        DB::table('admission_types')->insert([
            'id' => 2,
            'name' => 'Transfer interview',
            'description' => 'Formal admission interviews are required for transfer applicants'
        ]);
    }
}
