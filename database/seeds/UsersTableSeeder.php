<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'email'  => 'admissions@cloudhorizon.com',
            'password'  => Hash::make('admissions'),
            'first_name'  => 'super',
            'last_name'  => 'admin',
            'phone_number'  => '011557167',
            'role_id' => Role::ADMINISTRATOR
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'email'  => 'dusan@google.com',
            'password'  => Hash::make('secret'),
            'first_name'  => 'dusan',
            'last_name'  => 'veljovic',
            'phone_number'  => '011557167',
            'role_id' => Role::STUDENT
        ]);
    }
}
